import com.denyssh.matrix.Matrix;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MatrixMultiplicationTest {

    private Matrix testMatrixA;
    private Matrix testMatrixB;
    private Matrix expectedMatrix;

    @Before
    public void initTestValues () {
        testMatrixA = new Matrix(4);
        testMatrixA.setMatrixValues(new int[][]{{0, 0, 0, 0},
                                                {0, 1, 0, 0},
                                                {0, 0, 1, 1},
                                                {0, 1, 0, 1}});
        testMatrixB = new Matrix(4);
        testMatrixB.setMatrixValues(new int[][]{{1, 1, 1, 1},
                                                {0, 0, 1, 1},
                                                {1, 1, 1, 0},
                                                {1, 0, 0, 1}});
        expectedMatrix = new Matrix(4);
        expectedMatrix.setMatrixValues(new int[][]{{0, 0, 0, 0},
                                                {0, 0, 1, 1},
                                                {0, 1, 1, 1},
                                                {1, 0, 1, 0}});
    }

    @Test
    public void sequentialMultiplicationTest(){
        Matrix resultMatrix = testMatrixA.multiplySequentially(testMatrixB);
        Assert.assertEquals(expectedMatrix, resultMatrix);
    }

    @Test
    public void parallelMultiplicationTest(){
        Matrix resultMatrix = testMatrixA.parallelMultiply(testMatrixB);
        Assert.assertEquals(expectedMatrix, resultMatrix);
    }
}
