package com.denyssh.matrix;

public class BinaryMatrixFactory {

    /**
     * Build and return Matrix filled with random [0..1] values
     * @param size - matrix size
     * @return - new Matrix
     */
    static Matrix getMatrix(int size) {
        Matrix matrix = new Matrix(size);
        matrix.fillMatrixWithRandomValues();
        return matrix;
    }
}
