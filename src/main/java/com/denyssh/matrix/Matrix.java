package com.denyssh.matrix;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

/**
 * Matrix class is representing square matrix.
 * Have to be initialized with constructor
 */
public class Matrix {

    private int[][] matrixValues;
    private int matrixSize;

    public Matrix(int matrixSize) {
        this.matrixSize = matrixSize;
        this.matrixValues = new int[matrixSize][matrixSize];
    }

    /**
     * Fill matrix with random integers in range [0..1]
     */
    public void fillMatrixWithRandomValues() {
        Random random = new Random();
        for (int r = 0; r < matrixSize; r++) {
            for (int c = 0; c < matrixSize; c++) {
                matrixValues[r][c] = random.nextInt(2);
            }
        }
    }

    /**
     * Multiply two square matrices sequentially.
     * Use one current thread for execution.
     * Implemented for multiplying only binary matrices
     * @param matrixB - matrix for multiplying
     * @return - result of multiplying current matrix on matrixB
     */
    public Matrix multiplySequentially(Matrix matrixB) {
        Matrix resultMatrix = new Matrix(matrixSize);
        int[][] resultMatrixValues = new int[matrixSize][matrixSize];
        for(int r = 0; r < matrixSize; r++) {
            for (int c = 0; c < matrixSize; c++) {
                for (int i = 0; i < matrixSize; i++) {
                    resultMatrixValues[r][c] = (resultMatrixValues[r][c] + matrixValues[r][i] * matrixB.getMatrixValues()[i][c]) % 2;
                }
            }
        }
        resultMatrix.setMatrixValues(resultMatrixValues);
        return resultMatrix;
    }

    /**
     * Multiply two square matrices in parallel.
     * Use ForkJoinPool with maximum available CPU's threads for execution.
     * Implemented for multiplying only binary matrices
     * @param matrixB - matrix for multiplying
     * @return - result of multiplying current matrix on matrixB
     */

    public Matrix parallelMultiply(Matrix matrixB) {
        Matrix resultMatrix = new Matrix(matrixSize);
        ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());

        // each row calculation executed in new thread
        for(int r = 0; r < matrixSize; r++) {
            pool.submit(new MatrixMultiplyJob(this.matrixValues, matrixB, resultMatrix, r));
        }
        pool.shutdown();

        try {
            pool.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return resultMatrix;
    }

    public int[][] getMatrixValues() {
        return matrixValues;
    }

    public void setMatrixValues(int[][] matrixValues) {
        this.matrixValues = matrixValues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return matrixSize == matrix.matrixSize &&
                Arrays.deepEquals(matrixValues, matrix.matrixValues);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(matrixValues).forEach(arr -> {
            stringBuilder.append(Arrays.toString(arr)).append("\n");
        });
        return "Matrix{" +
                "matrixValues=" + stringBuilder +
                ", matrixSize=" + matrixSize +
                '}';
    }

    /**
     * Runnable inner class used for parallel multiplying execution
     */
    static class MatrixMultiplyJob implements Runnable {

        int[][] matrixValues;
        Matrix matrixB;
        Matrix resultMatrix;
        int r;

        MatrixMultiplyJob(int[][] matrixValues, Matrix matrixB, Matrix resultMatrix, int r) {
            this.matrixValues = matrixValues;
            this.matrixB = matrixB;
            this.resultMatrix = resultMatrix;
            this.r = r;
        }

        @Override
        public void run() {
            for (int c = 0; c < matrixValues.length; c++) {
                for (int i = 0; i < matrixValues.length; i++) {
                    resultMatrix.getMatrixValues()[r][c] =
                            (resultMatrix.getMatrixValues()[r][c] + matrixValues[r][i] * matrixB.getMatrixValues()[i][c]) % 2;
                }
            }
        }
    }
}
