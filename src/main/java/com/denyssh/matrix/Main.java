package com.denyssh.matrix;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        int matrixSize = readSizeArg(args);

        Matrix matrixA = BinaryMatrixFactory.getMatrix(matrixSize);
        Matrix matrixB = BinaryMatrixFactory.getMatrix(matrixSize);

        //multiply sequentially
        System.out.println(getFormattedCurrentTime() + " Sequential multiplying started... ");
        long time = System.currentTimeMillis();
        Matrix matrixC = matrixA.multiplySequentially(matrixB);
        System.out.println(getFormattedCurrentTime() + " Sequential multiplying finished. Execution time: " + (System.currentTimeMillis() - time));

        //multiply in parallel
        System.out.println(getFormattedCurrentTime() + " Parallel multiplying started... ");
        time = System.currentTimeMillis();
        Matrix matrixD = matrixA.parallelMultiply(matrixB);
        System.out.println(getFormattedCurrentTime() + " Parallel multiplying finished. Execution time: " + (System.currentTimeMillis() - time));
    }

    private static String getFormattedCurrentTime() {
        return new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
    }

    /**
     * Read matrices size from application arguments
     * @param args - provided arguments
     * @return - matrices size
     */
    private static int readSizeArg(String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("No matrix size provided. Enter size as first argument. Range is [1..10000]");
        }
        int size = Integer.parseUnsignedInt(args[0]);
        if (size > 10000) {
            throw new IllegalArgumentException("Matrix size argument is not valid. Range is [1..10000]");
        }
        return size;
    }
}
